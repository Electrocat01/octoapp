package de.crysxd.octoapp.base.datasource

data class GcodeFileDataSourceGroup(
    val dataSources: List<GcodeFileDataSource>
)