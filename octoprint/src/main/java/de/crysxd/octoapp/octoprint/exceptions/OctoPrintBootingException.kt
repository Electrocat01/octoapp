package de.crysxd.octoapp.octoprint.exceptions

import java.io.IOException
import java.lang.Exception

class OctoPrintBootingException() : OctoPrintException()